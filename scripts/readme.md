# Readme

The scripts folder should contain anonymous Apex which should be ran before or after a deployment to automate pre and
post deployment steps.

## Rerunnable scripts

When deploying to Production the scripts which are ran are only the scripts since the last successful deployment to
Production. This means that we do not run all of the scripts on every deployment, only the new ones. However, you
need to ensure that your scripts are rerunnable as there could be multiple pipelines which fail or do not get approved
to deploy to Production. This implication of having rerunnable scripts means that you have to check within your anonymous Apex whether to run the action or not.

## Naming convention

Due to the difference in behavior in filing systems, the order in which the scripts are ran and evaluated may be 
different and as such it is recommended to place incrementing number at the beginning of the filename to maintain the
correct desired order of execution.

When scripts fail to execute it can be useful also to include the user story number and a small description within
the filename to indicate what the script is doing. This will help the person to debug the anonymous Apex.

Below is an example of a pre-deployment script:

```
pre_0001_US-1234_sample_anon.apex
```

Below is an example of a post-deployment script:

```
post_0001_US-1234_sample_anon.apex
```